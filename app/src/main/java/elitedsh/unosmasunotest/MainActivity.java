package elitedsh.unosmasunotest;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import elitedsh.unosmasunotest.adapter.AdapterRates;
import elitedsh.unosmasunotest.model.mRates;
import elitedsh.unosmasunotest.util.ServerApi;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rv;
    private ArrayList<mRates> lista;
    private RecyclerView.Adapter adapter;

    private EditText etUsd;
    private TextView tvLastime,tvTitle;

    private ProgressBar pbLoading;

    private String TAG = "TAG";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etUsd =(EditText)findViewById(R.id.etUSD);
        tvTitle =(TextView) findViewById(R.id.tvTitle);
        tvLastime =(TextView) findViewById(R.id.tvLastTime);

        pbLoading = (ProgressBar)findViewById(R.id.pbLoading);

        rv =(RecyclerView)findViewById(R.id.rvRates);
        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

    }


    public void getLastRatesMoney(View view)
    {
        pbLoading.setVisibility(View.VISIBLE);
        //close keyboard
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        final String UsdLast = etUsd.getText().toString().trim();


        //validate empty
        if(!UsdLast.isEmpty() || !UsdLast.equals(""))
        {
            tvTitle.setVisibility(View.VISIBLE);
            tvLastime.setVisibility(View.VISIBLE);

            Log.v(TAG, "getLastRatesMoney");
            String URL = ServerApi.URL;
            lista = new ArrayList();
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.v(TAG, String.valueOf(response));

                    JSONObject jsonObject = null;
                    try {

                        tvLastime.setText("Ultima Actualización: "+response.getString("date"));

                        jsonObject = response.getJSONObject("rates");

                        int USDtoGBP  = getCalculate(UsdLast,jsonObject.getString("GBP"));
                        int USDtoEUR  = getCalculate(UsdLast,jsonObject.getString("EUR"));
                        int USDtoJPY  = getCalculate(UsdLast,jsonObject.getString("JPY"));
                        int USDtoBRL  = getCalculate(UsdLast,jsonObject.getString("BRL"));

                        lista.add(new mRates(USDtoGBP,USDtoEUR,USDtoJPY,USDtoBRL));

                        adapter = new AdapterRates(lista,MainActivity.this);
                        rv.setAdapter(adapter);
                        pbLoading.setVisibility(View.GONE);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pbLoading.setVisibility(View.GONE);
                    alert("Al parecer no tienes internet, verifícalo");

                }
            });

            Volley.newRequestQueue(this).add(jsonObjectRequest);


        }
        else
        {
            alert("Agrega un Valor en USD, Para calcular...");
            pbLoading.setVisibility(View.GONE);
        }




    }

    public int getCalculate(String USD,String convertion)
    {
        Log.v(TAG,"USB:"+USD+";Conversion:"+convertion);

        float Usd = Integer.parseInt(USD);
        float Convertion = Float.parseFloat(convertion);
        float valueF = Usd*Convertion;
        int value = (int)valueF;

        return value;
    }

    public void alert(String msg)
    {
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
    }


}
