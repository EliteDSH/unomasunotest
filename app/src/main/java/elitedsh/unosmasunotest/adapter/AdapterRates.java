package elitedsh.unosmasunotest.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import elitedsh.unosmasunotest.R;
import elitedsh.unosmasunotest.model.mRates;

/**
 * Created by julianx1 on 3/8/18.
 */

public class AdapterRates extends RecyclerView.Adapter<AdapterRates.ViewHolder> {

    private ArrayList<mRates> mDatos;
    private String TAG = "TAG";
    private Context context;

    public AdapterRates(ArrayList<mRates> mDatos,Context context) {
        this.mDatos = mDatos;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        //function
        private final TextView GBP;
        private final TextView EUR;
        private final TextView JPY;
        private final TextView BRL;


        public ViewHolder(View itemView) {
            super(itemView);

            GBP = (TextView) itemView.findViewById(R.id.tvGBP);
            EUR = (TextView) itemView.findViewById(R.id.tvEUR);
            JPY = (TextView) itemView.findViewById(R.id.tvJPY);
            BRL = (TextView) itemView.findViewById(R.id.tvBRL);

        }
    }


    @Override
    public AdapterRates.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cv_rates, parent, false);
       return new AdapterRates.ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        mRates rates = mDatos.get(position);

        holder.GBP.setText("£"+String.valueOf(rates.getGBP()));
        holder.EUR.setText("€"+String.valueOf(rates.getEUR()));
        holder.JPY.setText("¥"+String.valueOf(rates.getJPY()));
        holder.BRL.setText(" R"+String.valueOf(rates.getBRL()));

    }

    @Override
    public int getItemCount() {
        return mDatos.size();
    }

}
