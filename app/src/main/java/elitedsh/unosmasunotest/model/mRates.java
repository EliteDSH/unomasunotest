package elitedsh.unosmasunotest.model;

/**
 * Created by julianx1 on 3/8/18.
 */

public class mRates {

    private int GBP;
    private int EUR;
    private int JPY;
    private int BRL;


    public mRates(int GBP, int EUR, int JPY, int BRL) {
        this.GBP = GBP;
        this.EUR = EUR;
        this.JPY = JPY;
        this.BRL = BRL;
    }

    public int getGBP() {
        return GBP;
    }

    public void setGBP(int GBP) {
        this.GBP = GBP;
    }

    public int getEUR() {
        return EUR;
    }

    public void setEUR(int EUR) {
        this.EUR = EUR;
    }

    public int getJPY() {
        return JPY;
    }

    public void setJPY(int JPY) {
        this.JPY = JPY;
    }

    public int getBRL() {
        return BRL;
    }

    public void setBRL(int BRL) {
        this.BRL = BRL;
    }
}
